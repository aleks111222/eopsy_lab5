#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <sys/wait.h>

using namespace std;

key_t key = 1001;
int nsems = 5;
int semflg = IPC_CREAT | 0666;
int semid;

void grab_forks_right(int right_fork_id) {

	int left_fork_id = right_fork_id - 1;

	right_fork_id %= 5;

	struct sembuf sops[4];

	sops[0].sem_num = right_fork_id;
	sops[0].sem_op = 0;
	sops[0].sem_flg = SEM_UNDO;

	sops[1].sem_num = right_fork_id;
	sops[1].sem_op = 1;
	sops[1].sem_flg = SEM_UNDO;

	sleep(1);

	sops[2].sem_num = left_fork_id;
	sops[2].sem_op = 0;
	sops[2].sem_flg = SEM_UNDO;
	
	sops[3].sem_num = left_fork_id;
	sops[3].sem_op = 1;
	sops[3].sem_flg = SEM_UNDO;

	if (semop(semid, sops, 4) == -1) {
		perror("semop failure");
		exit(EXIT_FAILURE);
	}
}

void grab_forks_left(int left_fork_id) {

	int right_fork_id = left_fork_id + 1;

	right_fork_id %= 5;

	struct sembuf sops[4];

	sops[0].sem_num = left_fork_id;
	sops[0].sem_op = 0;
	sops[0].sem_flg = SEM_UNDO;

	sops[1].sem_num = left_fork_id;
	sops[1].sem_op = 1;
	sops[1].sem_flg = SEM_UNDO;

	sleep(1);

	sops[2].sem_num = right_fork_id;
	sops[2].sem_op = 0;
	sops[2].sem_flg = SEM_UNDO;
	
	sops[3].sem_num = right_fork_id;
	sops[3].sem_op = 1;
	sops[3].sem_flg = SEM_UNDO;

	if (semop(semid, sops, 4) == -1) {
		perror("semop failure");
		exit(EXIT_FAILURE);
	}
}

void put_away_forks(int left_fork_id) {

	int right_fork_id = left_fork_id + 1;

	right_fork_id %= 5;

	struct sembuf sops[2];

	sops[0].sem_num = left_fork_id;
	sops[0].sem_op = -1;
	sops[0].sem_flg = SEM_UNDO;
	
	sops[1].sem_num = right_fork_id;
	sops[1].sem_op = -1;
	sops[1].sem_flg = SEM_UNDO;

	if (semop(semid, sops, 2) == -1) {
		perror("semop failure");
		exit(EXIT_FAILURE);
	}
}

int count_dinings[5];

int main() {

	if ((semid = semget(key, nsems, semflg)) == -1) {
		perror("semget failure");
		exit(EXIT_FAILURE);
	}

	bool myFirstTime = true;

	for (int i = 0; i < 5; i++) {

		srand(time(NULL) + i);
		
		pid_t philosopher = fork();

		bool right_handed = true;

		if (i == 0) right_handed = false;

		if (philosopher < 0)
			perror("\nCannot create the philosopher");
			
		else if (philosopher == 0) {
		
			while(true) {
				
				int thinking_time = rand() % 2 + 1;
				//cout << "Philosopher " << i << " wants to think" << endl;
				if(!myFirstTime) {
					put_away_forks(i);
					count_dinings[i]++;
					cout << "Philosopher " << i << " has eaten " << count_dinings[i] << " times" << endl;
				}
				myFirstTime = false;
				//cout << "Philosopher " << i << " is thinking" << endl;
				sleep (thinking_time);

				int eating_time = rand() % 2 + 1;
				//cout << "Philosopher " << i << " wants to eat" << endl;
				
				if (right_handed)
					grab_forks_right(i + 1);
				else
					grab_forks_left(i);
					
				//cout << "Philosopher " << i << " is eating" << endl;
				sleep(eating_time);
			}
		}
	}

	for (int i = 0; i < 5; i++) {
	
		wait(NULL);
	}
}
